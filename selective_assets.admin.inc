<?php

/**
 * @file
 * Administration interface form and actions
 */

include_once "includes/selective_assets.inc";

/**
 * Returns a listing of selective asset rules.
 *
 * TODO: When filter key passed, perform a standard search on the given key,
 * and return the list of matching rules.
 */
function selective_assets_admin_overview() {
  $header = array();
  $header[] = array('data' => t('URL'), 'field' => 'url', 'sort' => 'asc');
  $header[] = array('data' => t('Include'), 'field' => 'include');
  $header[] = array('data' => t('Exclude'), 'field' => 'exclude');

  $header[] = array('data' => t('Operations'));

  $query = db_select('selective_assets')->extend('PagerDefault')->extend('TableSort');

  $result = $query
    ->fields('selective_assets')
    ->orderByHeader($header)
    ->limit(50)
    ->execute();

  $rows = array();
  $destination = drupal_get_destination();
  foreach ($result as $data) {
    $row = array();
    $row['data']['url'] = $data->url;
    $row['data']['include'] = $data->include;
    $row['data']['exclude'] = $data->exclude;

    $operations = array();
    $operations['edit'] = array(
      'title' => t('edit'),
      'href' => "admin/config/selective_assets/edit/$data->pid",
      'query' => $destination,
    );
    $operations['delete'] = array(
      'title' => t('delete'),
      'href' => "admin/config/selective_assets/delete/$data->pid",
      'query' => $destination,
    );
    $row['data']['operations'] = array(
      'data' => array(
        '#theme' => 'links',
        '#links' => $operations,
        '#attributes' => array('class' => array('links', 'inline', 'nowrap')),
      ),
    );

    $rows[] = $row;
  }

  $build['selective_assets_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No rules available. <a href="@link">Add an asset rule</a>.', array('@link' => url('admin/config/selective_assets/add'))),
  );
  $build['selective_assets_pager'] = array('#theme' => 'pager');

  return $build;
}

/**
 * Page callback: Returns a form creating or editing a rule.
 *
 * @param int $pid
 *   The Rule ID.
 *
 * @return mixed
 *   A form for adding or editing a rule.
 *
 * @see selective_assets_menu()
 */
function selective_assets_admin_edit($pid = NULL) {
  if ($pid) {
    $output = drupal_get_form('selective_assets_admin_form', intval($pid));
  }
  else {
    $output = drupal_get_form('selective_assets_admin_form');
  }
  return $output;
}

/**
 * Form constructor for the selective assets administration form.
 *
 * @param int $pid
 *   The rule ID.
 *
 * @ingroup forms
 * @see selective_assets_admin_form_validate()
 * @see selective_assets_admin_form_submit()
 * @see selective_assets_admin_form_delete_submit()
 */
function selective_assets_admin_form($form, &$form_state, $pid = NULL) {
  $rule = array('pid' => NULL, 'url' => '', 'include' => '', 'exclude' => '');
  if (is_int($pid)) {
    $rule = selective_assets_load($pid);
  }

  $form['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#default_value' => $rule['url'],
    '#maxlength' => 255,
    '#size' => 45,
    '#description' => t('Specify the existing path to apply the rule.'),
    '#field_prefix' => url(NULL, array('absolute' => TRUE)) .
    (variable_get('clean_url', 0) ? '' : '?q='),
    '#required' => TRUE,
  );
  $form['include'] = array(
    '#type' => 'textarea',
    '#title' => t('Include assets'),
    '#default_value' => $rule['include'],
    '#rows' => 5,
    '#description' => t(''),
    '#required' => FALSE,
  );
  $form['exclude'] = array(
    '#type' => 'textarea',
    '#title' => t('Exclude assets'),
    '#default_value' => $rule['exclude'],
    '#rows' => 5,
    '#description' => t(''),
    '#required' => FALSE,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  if ($rule['pid']) {
    $form['pid'] = array(
      '#type' => 'hidden',
      '#value' => $rule['pid'],
    );
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('selective_assets_admin_form_delete_submit'),
    );
  }

  return $form;
}


/**
 * Form submission handler for the 'Delete' button on path_admin_form().
 *
 * @see path_admin_form_validate()
 * @see path_admin_form_submit()
 */
function selective_assets_admin_form_delete_submit($form, &$form_state) {
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $form_state['redirect'] = array('admin/config/selective_assets/delete/' .
    $form_state['values']['pid'], array('query' => $destination));
}


/**
 * Form submission handler for selective_assets_admin_form().
 *
 * @see selective_assets_admin_form_delete_submit()
 */
function selective_assets_admin_form_submit($form, &$form_state) {
  // Remove unnecessary values.
  form_state_values_clean($form_state);

  foreach ($form_state['values'] as $k => $v) {
    $form_state['values'][$k] = strip_tags($v);
  }

  selective_assets_save($form_state['values']);

  drupal_set_message(t('The rule has been saved.'));
  $form_state['redirect'] = 'admin/config/selective_assets';
}

/**
 * Form constructor for the path deletion form.
 *
 * @param int $pid
 *   The rule ID.
 *
 * @see selective_assets_admin_delete_confirm_submit()
 */
function selective_assets_admin_delete_confirm($form, &$form_state, $pid) {
  if (user_access('administer selective_assets')) {
    if ($pid != NULL) {
      $rule = selective_assets_load(intval($pid));
    }

    $form_state['pid'] = intval($pid);

    return confirm_form(
        $form,
        t('Are you sure you want to delete rule for %title?',
        array('%title' => $rule['url'])),
        'admin/config/selective_assets'
    );
  }
  return array();
}

/**
 * Form submission handler for selective_assets_admin_delete_confirm().
 */
function selective_assets_admin_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    selective_assets_delete($form_state['pid']);
    $form_state['redirect'] = 'admin/config/selective_assets';
  }
}
