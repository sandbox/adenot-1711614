<?php

/**
 * @file
 * Helper DB functions: load, save and delete rules.
 */

/**
 * Fetch a specific rule from the database.
 *
 * @param mixed $conditions
 *   A string representing the source, a number representing the pid, or an
 *   array of query conditions.
 *
 * @return mixed
 *   FALSE if no alias was found or an associative array containing the
 *   following keys:
 *   - pid: Unique rule identifier.
 */
function selective_assets_load($conditions) {
  if (is_numeric($conditions)) {
    $conditions = array('pid' => $conditions);
  }
  $select = db_select('selective_assets');
  foreach ($conditions as $field => $value) {
    $select->condition($field, $value);
  }
  return $select
         ->fields('selective_assets')
         ->execute()
         ->fetchAssoc();
}

/**
 * Save a rule to the database.
 *
 * @param array $rule
 *   An associative array containing the following key:
 *   - pid: Unique rule identifier.
 */
function selective_assets_save(&$rule) {

  // Load the stored rule, if any.
  if (!empty($rule['pid']) && !isset($rule['original'])) {
    $rule['original'] = selective_assets_load($rule['pid']);
  }

  $rule['include'] = strip_tags($rule['include']);
  $rule['exclude'] = strip_tags($rule['exclude']);

  if (empty($rule['pid'])) {
    drupal_write_record('selective_assets', $rule);
  }
  else {
    drupal_write_record('selective_assets', $rule, array('pid'));
  }

  // Clear internal properties.
  unset($rule['original']);

  // Clear the static alias cache.
  // drupal_clear_path_cache($rule['url']);
}

/**
 * Delete a rule.
 *
 * @param mixed $criteria
 *   A number representing the pid or an array of criteria.
 */
function selective_assets_delete($criteria) {
  if (!is_array($criteria)) {
    $criteria = array('pid' => $criteria);
  }
  $path = selective_assets_load($criteria);
  $query = db_delete('selective_assets');
  foreach ($criteria as $field => $value) {
    $query->condition($field, $value);
  }
  $query->execute();
}
