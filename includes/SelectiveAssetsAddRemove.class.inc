<?php

/**
 * @file
 * Class SelectiveAssetsAddRemove, provide in page functionality for deciding
 * whether or not to show assets defined by admin's rules
 */


/**
 * SelectiveAssetsAddRemove is a class to manipulate a drupaal node headers
 *
 * @author Allan Denot
 */
class SelectiveAssetsAddRemove {

  /**
   * stores own instance, singleton pattern
   * @var SelectiveAssetsAddRemove
   */
  protected static $instance = NULL;

  /**
   * rules gathered from the database
   * @var DatabaseStatementInterface
   */
  protected $rules;

  /** stores current page path
   * @var string
   */
  protected $path;


  public function __construct() {
    $this->path = drupal_strtolower(drupal_get_path_alias($_GET['q']));
    $rules = db_query('SELECT url, include, exclude FROM {selective_assets}');
    $this->rules = array();

    foreach ($rules as $rule) {
      if (!$this->pageMatch($rule)) {
        continue;
      }

      $this->rules[] = $rule;

    }
  }

  protected function pageMatch($rule) {
    $pages = drupal_strtolower($rule->url);
    $page_match = drupal_match_path($this->path, $pages);
    if ($this->path != $_GET['q']) {
      $page_match = $page_match || drupal_match_path($_GET['q'], $pages);
    }
    return $page_match;
  }

  public function addAssets(&$vars) {
    foreach ($this->rules as $rule) {
      $include = explode("\n", $rule->include);
      foreach ($include as $asset) {
        $asset = trim($asset);
        if (drupal_substr($asset, -3) == ".js") {
          drupal_add_js($asset, 'file');

        }
        elseif (drupal_substr($asset, -4) == ".css") {
          $css_type = "file";
          if (drupal_substr($asset, 0, 5) == "http:" || substr($asset, 0, 6) == "https:") {
            $css_type = "external";
          }

          drupal_add_css($asset, array('group' => CSS_DEFAULT, 'type' => $css_type));
        }

      }
    }
  }

  public function removeCSS(&$css) {
    foreach ($this->rules as $rule) {
      $exclude = explode("\n", $rule->exclude);

      foreach ($exclude as $asset) {
        $asset = trim($asset);

        if (drupal_substr($asset, -4) != ".css") {
          continue;
        }

        foreach ($css as $sheet => $data) {
          if (strpos($sheet, $asset) !== FALSE) {
            unset($css[$sheet]);
            // TODO: cache this.
          }
        }
      }
    }
  }

  public function removeJavascript(&$javascript) {
    foreach ($this->rules as $rule) {
      $exclude = explode("\n", $rule->exclude);

      foreach ($exclude as $asset) {
        $asset = trim($asset);

        if (drupal_substr($asset, -3) != ".js") {
          continue;
        }

        foreach ($javascript as $script => $data) {
          if (strpos($script, $asset) !== FALSE) {
            unset($javascript[$script]);
            // TODO: cache this.
          }
        }
      }
    }
  }


  public function getRules() {
    return $this->rules;
  }

  public static function getInstance() {
    if (self::$instance === NULL) {
      self::$instance = new SelectiveAssetsAddRemove();
    }
    return self::$instance;
  }
}
