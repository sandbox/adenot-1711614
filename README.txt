*******************************************************************************
                     D R U P A L    M O D U L E
*******************************************************************************
Name: Selective Assets Module
Author: Allan Denot <adenot_at_gmaildotcom>
Drupal: 7
*******************************************************************************

DESCRIPTION:

This module allows you to select which assets (CSS and JS) files you want to 
include or exclude in individual pages.

The idea is to allow developers to include/exclude CSS and JS files to a page 
without having to add to the theme or programmatically.

My motivation for developing this module was to reduce Drupal pages footprint 
by removing unnecessary JS/CSS from pages that don't use them.

This module is new (07/2012), therefore its implementation is still very 
simple and bug prone. Please let me know what features you would like to see 
and if you find any bugs.

USAGE:

After installing and enabling the module, you can specify assets to be added or 
removed from individual pages.

1. Go to the module admin page /admin/config/selective_assets

2. Click "Add Rule"

3a. Specify the path that this rule will apply, wildcards are allowed.
    Eg.: admin/* or content/test or events/*

3b. Enter assets (one per line) that you want to include in that page
    You can specify external assets by starting with http:// or https://
    Eg. (local): /sites/all/themes/mytheme/style.css or 
    Eg. (external): http://code.jquery.com/jquery-1.7.2.min.js

3c. Enter assets (one per line) that you want to exclude from that page
    Assets to remove can be partial matchs, preferably use the filename.
    Eg.: search.css or /sites/all/themes/mytheme/css/search.css

4. Save

5. Clear all caches 
